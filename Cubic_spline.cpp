#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include "lib/matrix.hpp"
#include "lib/metod_Gaussa.hpp"
using namespace std;
//const int MAX_SIZE = 50;
const double diff_eps = 0.1;
const double pi = 3.14159265;

double y(double h[MAX_SIZE], double a[MAX_SIZE],
         double b[MAX_SIZE], double d[MAX_SIZE],
         double x[MAX_SIZE], double f[MAX_SIZE],
         double c[MAX_SIZE], int i, double X)
{
  return a[i] + b[i]*(X - x[i-1]) + c[i]*pow((X - x[i-1]),2) + d[i]*pow((X - x[i-1]),3);
}

void splines(double h[MAX_SIZE], double a[MAX_SIZE],
             double b[MAX_SIZE], double d[MAX_SIZE],
             double x[MAX_SIZE], double f[MAX_SIZE],
             double c[MAX_SIZE], int n)
{
  int i;
  for (i = 1; i < n+1; i++)
    a[i] = f[i-1];

  for (i = 1; i < n+1; i++)
  {
    if (i == n)
      b[i] = (((f[i] - f[i-1])/h[i]) - (2.0/3)*h[i]*c[i]);
    else
      b[i] = ((f[i] - f[i-1])/h[i]) - (1.0/3)*h[i]*(c[i+1] + 2*c[i]);
  }
  for (i = 1; i < n+1; i++)
  {
    if (i == n)
      d[i] = -(c[i])/3*h[i];
    else
      d[i] = (c[i+1] - c[i])/3*h[i];
  }
  print_vector_to_file(a, cout, n, " a");
  print_vector_to_file(b, cout, n, " b");
  print_vector_to_file(c, cout, n, " c");
  print_vector_to_file(d, cout, n, " d");
  print_vector_to_file(f, cout, n, " f");
  print_vector_to_file(h, cout, n, " h");
}



void my_system(double matrix_c[MAX_SIZE][MAX_SIZE], double h[MAX_SIZE],
               double f[MAX_SIZE], double x[MAX_SIZE], int n)
{
  for (int i = 1; i < n+1; i++)
    h[i] = (x[i] - x[i-1]);

  matrix_c[0][0] = 2*(h[1] + h[2]);
  matrix_c[0][1] = h[1];
  matrix_c[0][3] = 3*(((f[2]-f[1])/h[2]) - ((f[1] - f[0])/h[1]));
  matrix_c[1][0] = h[2];
  matrix_c[1][1] = 2*(h[3] + h[4]);
  matrix_c[1][2] = h[3];
  matrix_c[1][3] = 3*(((f[3] - f[2])/h[3]) - ((f[2] - f[1])/h[2]));
  matrix_c[2][1] = h[2];
  matrix_c[2][2] = 2*(h[3] + h[4]);
  matrix_c[2][3] = 3*(((f[4] - f[3])/h[4]) - ((f[3] - f[2])/h[3]));
  //print_matrix_to_file(matrix_c, cout, 3, 4);
}

void shift_zero_to_vector(double c[MAX_BUDLO], int n)
{
  for (int i = n; i >= 0; i--)
    c[i+2] = c[i];
  c[1] = 0;
}

int main()
{
  double matrix_c[MAX_SIZE][MAX_SIZE], x[MAX_SIZE], f[MAX_SIZE],
         h[MAX_SIZE], a[MAX_SIZE], b[MAX_SIZE],
         d[MAX_SIZE], c[MAX_SIZE], X = 1.5;
  int n = 5;
  x[0] = 0, x[1] = 1, x[2] = 2, x[3] = 3, x[4] = 4;
  f[0] = 0, f[1] = 0.86603, f[2] = 0.5, f[3] = 0.0, f[4] = -0.5;
  //f[0] = 0, f[1] = 1.8415, f[2] = 2.9093, f[3] = 3.1411, f[4] = 3.2432;
  //c[1] = 0, c[2] = -0.961340, c[3] = 0.141981, c[4] = -0.037295;

  my_system(matrix_c, h, f, x, 5);
  solution_of_the_system(matrix_c, 3, c);
  shift_zero_to_vector(c, 3);
  //print_vector_to_file(c, cout, 4);
  splines(h, a, b, d, x, f, c, 5);
  cout << "y = " << y(h, a, b, d, x, f, c, 2, X) << endl;

}
