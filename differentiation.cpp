#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
using namespace std;
const int MAX_SIZE = 50;
const double diff_eps = 0.1;
const double pi = 3.14159265;

double first_diff(double x[MAX_SIZE], double y[MAX_SIZE], int num)
{
  return (y[num] - y[num-1])/(x[num] - x[num-1]);
}

double first_diff_l_and_r(double x[MAX_SIZE], double y[MAX_SIZE], int num)
{
  return first_diff(x, y, num) + (first_diff(x, y, num+1) - first_diff(x, y, num))*(2*x[num]-x[num]-x[num+1])/(x[num+1]-x[num-1]);
}

double second_diff(double x[MAX_SIZE], double y[MAX_SIZE], int num)
{
  return 2*((first_diff(x, y, num+1) - first_diff(x, y, num))/(x[num+1] - x[num-1]));
}

int main()
{
  double x[MAX_SIZE], y[MAX_SIZE];
  int num = 2;
  // x[0] = 0.0,  x[1] = 0.1,  x[2] = 0.2,  x[3] = 0.3,  x[4] = 0.4;
  // y[0] = 1.0,  y[1] = 1.1052,  y[2] = 1.2214,  y[3] = 1.3499,  y[4] = 1.4918;

  x[0] = -1.0,  x[1] = 0.0,  x[2] = 1.0,  x[3] = 2.0,  x[4] = 3.0;
  y[0] = -0.5,  y[1] = 0.0,  y[2] = 0.5,  y[3] = 0.86603,  y[4] = 1.0;

  cout << "first_diff_left = " << first_diff(x, y, num) << endl;
  cout << "first_diff_rigth = " << first_diff(x, y, num+1) << endl;
  cout << "first_diff_l_and_r = " << first_diff_l_and_r(x, y, num+1) << endl;
  cout << "second_diff = " << second_diff(x, y, num) << endl;

}
