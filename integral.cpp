#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include "lib/matrix.hpp"
using namespace std;
//const int MAX_SIZE = 50;
const double diff_eps = 0.1;
const double pi = 3.14159265;

double fun_y(double x)
{
  return x/(pow((3*x + 4), 2));
}

double metod_rectangle(double x[MAX_SIZE], double h, int n)
{
  double temp = 0;
  for (int i = 1; i < n; i++)
    temp += h*fun_y((x[i-1] + x[i])/2);
  return temp;
}

double metod_trap(double x[MAX_SIZE], double h, int n)
{
  double temp = 0;
  for (int i = 1; i < n-1; i++)
    temp += h*(fun_y(x[i]));
  return temp + h*fun_y(x[0])/2 + h*fun_y(x[n-1])/2;
}

double metod_simps(double x[MAX_SIZE], double h, int n)
{
  int oper;
  double temp;
  for(int i = 1; i < n-1; i++)
  {
    if ((i%2) == 0)
      oper = 2;
    else
      oper = 4;

    temp += oper*fun_y(x[i]);
  }
  return (h/3)*(fun_y(x[0]) + fun_y(x[n-1]) + temp);
}

double metor_rrr(double a, double b, double h1, double h2, int p)
{
  return a + (a - b)/(pow((h2/h1),p) - 1);
}

double error(double a, double b)
{
  return a-b;
}

int main()
{
  double rect[MAX_SIZE], x1[MAX_SIZE], x2[MAX_SIZE], x3[MAX_SIZE], y[MAX_SIZE],
  trap[MAX_SIZE], simps[MAX_SIZE], h1 = 0.5, h2 = 0.25;
  int n1 = 5, n2 = 9, p = 4;


  x1[0] = -1.0,  x1[1] = -0.5,  x1[2] = 0.0,  x1[3] = 0.5,  x1[4] = 1.0;
  cout << endl << "h = 0.5" << endl;
  cout << "metod_rectangle = " << metod_rectangle(x1, h1, n1) << endl;
  cout << "metod_trap = " << metod_trap(x1, h1, n1) << endl;
  cout << "metod_simps = " << metod_simps(x1, h1, n1) << endl;

  cout << endl << "h = 0.25" << endl;
  x2[0] = -1.0,  x2[2] = -0.5,  x2[4] = 0.0,  x2[6] = 0.5,  x2[8] = 1.0;
  x2[1] = -0.75,  x2[3] = -0.25,  x2[5] = 0.25,  x2[7] = 0.75;
  cout << "metod_rectangle = " << metod_rectangle(x2, h2, n2) << endl;
  cout << "metod_trap = " << metod_trap(x2, h2, n2) << endl;
  cout << "metod_simps = " << metod_simps(x2, h2, n2) << endl;

  cout << endl << "Metod Runge Romberga Richarda " << endl;
  cout << "metod_rectangle = " << metor_rrr(metod_rectangle(x1, h1, n1), metod_rectangle(x2, h2, n2), h1, h2, 2) << endl;
  cout << "metod_trap = " << metor_rrr(metod_trap(x1, h1, n1), metod_trap(x2, h2, n2), h1, h2, 2) << endl;
  cout << "metod_simps = " << metor_rrr(metod_simps(x1, h1, n1), metod_simps(x2, h2, n2), h1, h2, 4) << endl;

  
}
