#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
using namespace std;
const int MAX_SIZE = 50;
const double diff_eps = 0.1;

double f1(double x1, double x2)
{
    //return 0.3 - 0.1*pow(x1,2) - 0.2*pow(x2,2);
    //return pow(x1,2)*x2 + 9*x2 - 27;
    return sqrt(9 - pow((x2 - 3.0/2), 2)) + 3.0/2;
}

double f2(double x1, double x2)
{
    //return 0.7 - 0.2*pow(x1,2) + 0.1*x1*x2;
    //return pow(x1,2) - 3*x1 + 9.0/4 + pow(x2,2) - 3*x2 + 9.0/4 - 9;
    return 27/(pow(x1, 2) + 9);
}

double diff_f1_x1(double x1, double x2)
{
    //return - 0.2*x1;
    //return 2*x1*x2;
    return 0;
}

double diff_f1_x2(double x1, double x2)
{
    //return - 0.4*x2;
    //return (1.0/3)*(2*x2 - 3);
    return (x2 - 3.0/2)/(sqrt(9 - pow((x2 - 3.0/2), 2)));
}

double diff_f2_x1(double x1, double x2)
{
    //return - 0.4*x1 + 0.1*x2;
    //return 2*x1 - 3;
    return (27*2*x1)/(pow((pow(x1, 2) + 9), 2));
}

double diff_f2_x2(double x1, double x2)
{
    //return 0.1*x1;
    //return 2*x2 - 3;
    return 0.0;
}

double fun_q(double x1, double x2)
{
  double df1, df2;
  df1 = abs(diff_f1_x1(x1,x2)) + abs(diff_f1_x2(x1,x2));
  df2 = abs(diff_f2_x1(x1,x2)) + abs(diff_f2_x2(x1,x2));
  if (df1 > df2)
    return df1/(1.0 - df1);
  else
    return df2/(1.0 - df2);
}

bool check_eps(double x1[MAX_SIZE], double x2[MAX_SIZE], int k, double eps)
{
  double abs1 = abs(x1[k+1]-x1[k]), abs2 = abs(x2[k+1]-x2[k]);
  return !((abs1 > abs2) ? (fun_q(x1[k+1],x2[k+1])*abs1 < eps) : (fun_q(x1[k+1],x2[k+1])*abs2 < eps));
}


double metod_Newtone(double x1[MAX_SIZE], double x2[MAX_SIZE], double eps)
{
  int i = 0;

  do {
    x1[i+1] = f1(x1[i], x2[i]);
    x2[i+1] = f2(x1[i], x2[i]);

    cout << "iteration № " << i+1 << endl;

    cout << "x1 = " << x1[i] << endl;
    cout << "x2 = " << x2[i] << endl;

    cout << "f1 = " << f1(x1[i], x2[i]) << endl;
    cout << "f2 = " << f2(x1[i], x2[i]) << endl << endl;

    // cout << "diff_f1_x1 = " << diff_f1_x1(x1[i],x2[i]) << endl;
    // cout << "diff_f1_x2 = " << diff_f1_x2(x1[i],x2[i]) << endl;
    // cout << "diff_f2_x1 = " << diff_f2_x1(x1[i],x2[i]) << endl;
    // cout << "diff_f2_x2 = " << diff_f2_x2(x1[i],x2[i]) << endl;

    i++;
  } while(check_eps(x1, x2, i-1, eps) && i < 100);
  cout << "x1 = " << x1[i] << " x2 = " << x2[i] << endl;
}

int main()
{
  double x1[MAX_SIZE], x2[MAX_SIZE];
  double eps;
  cout << "enter the number eps = ";
  std::cin >> eps;

  x1[0] = 4.5;
  x2[0] = 1;
  metod_Newtone(x1, x2, eps);

}
