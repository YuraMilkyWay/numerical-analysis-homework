#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
using namespace std;
const int MAX_SIZE = 50;

double g_x(double x)
{
    //return (log(4 - 3*x)/2);
    return -sqrt(log(x + 2));
}


double metod_simple_iteration(double x[MAX_SIZE], double eps)
{
  int i = 0;
  do {
    x[i+1] = g_x(x[i]);
    i++;
  } while(!(abs(x[i] - x[i-1]) <= eps));
  cout << "x(k) = " << x[i] << /*" g(x(k)) = " << g_x(x[i]) <<*/ endl;
}

//((0.64)/(1 - 0.64))

int main()
{
  double x1[MAX_SIZE], x2[MAX_SIZE];
  double eps;
  std::cin >> eps;
  x1[0] = -0.6;
  x2[0] = 0;
  //cout << endl << "first root" << metod_simple_iteration(x1, eps) << endl;
  cout << "first_root" << endl;
  metod_simple_iteration(x1, eps);
  //cout << endl << "second root" << metod_simple_iteration(x2, eps) << endl;
  cout << "second_root" << endl;
  metod_simple_iteration(x2, eps);
}
