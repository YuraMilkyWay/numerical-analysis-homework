#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include "lib/matrix.hpp"
using namespace std;
const double eps = 0.01;

void make_equivalent(double A[MAX_SIZE][MAX_SIZE], int n)
{
  for(int i = 0; i < n; i++)
  {
    double diviser = A[i][i];
    if (diviser == 0)
      continue;
    else
      for(int j = 0; j < n + 1; j++)
      {
        A[i][j] = (i == j) ? 0 : (A[i][j] / diviser);
        if (j < n)
          A[i][j] = -A[i][j];
      }
  }
}

double max_string_sum_in_matrix(double A[MAX_SIZE][MAX_SIZE], int n, int m)
{
  double max_string = 0;
  for(int i = 0; i < n; i++)
  {
    double sum = 0;
    for(int j = 0; j < m; j++)
      sum += A[i][j];
    sum = abs(sum);
    if (sum > max_string)
      max_string = sum;
  }
  return max_string;
}

void separate_last_column(double A[MAX_SIZE][MAX_SIZE], double B[MAX_SIZE][MAX_SIZE], int n)
{
  for(int i = 0; i < n; i++)
  {
    B[i][0] = A[i][n];
    A[i][n] = 0;
  }
}

double check_eps(double X[MAX_SIZE][MAX_SIZE], double X0[MAX_SIZE][MAX_SIZE], int n, double eps, double alpha)
{
  double SUB[MAX_SIZE][MAX_SIZE];
  subtract_matrixes(X, X0, SUB, n, 1);
  // cout << max_string_sum_in_matrix(SUB, n, 1) << ", ";``
  return (alpha/(1 - alpha) * max_string_sum_in_matrix(SUB, n, 1));
}

int simple_iterations_method(double A[MAX_SIZE][MAX_SIZE], double X[MAX_SIZE][MAX_SIZE], int n, double eps)
{
  double B[MAX_SIZE][MAX_SIZE], R[MAX_SIZE][MAX_SIZE], X0[MAX_SIZE][MAX_SIZE], alpha;
  make_equivalent(A, n);
  int counter = 0;
  if ((alpha = max_string_sum_in_matrix(A, n, n)) >= 1)
  {
    cout << "Не соблюдено достаточное условие сходимости матрицы";
    return 1;
  }
  separate_last_column(A, B, n);
  copy_matrixes(X, B, n, 1);
  do
  {
    copy_matrixes(X0, X, n, 1);
    multiply_matrixes(A, X, R, n, n, n, 1);
    add_matrixes(B, R, X, n, 1);
    cout << "eps: " << check_eps(X, X0, n, eps, alpha) << endl;
    print_matrix_to_file(X, cout, n, 1, "X");
  }while(check_eps(X, X0, n, eps, alpha) > eps);
}

int main()
{
  double A[MAX_SIZE][MAX_SIZE], X[MAX_SIZE][MAX_SIZE];
  ifstream fin("input.txt");
  ofstream fout("output.txt");

  fout.precision(5);
  cout.precision(5);

  int n = input_matrix(A, fin);
  simple_iterations_method(A, X, n, eps);
  print_matrix_to_file(X, cout, n, 1, "X");
  return 0;
}
