#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
using namespace std;
const int MAX_SIZE = 50;

double funct(double x)
{
    return ((exp(2*x)) + 3*x - 4);
    //return (log(x + 2) - pow(x, 2));
}

double first_derivative(double x)
{
  double h = 0.1;
  return ((funct(x + h) - funct(x - h)) / (2*h));
}


double metod_Newtone(double x[MAX_SIZE], double eps)
{
  int i = -1;
  do {
    i++;
    x[i+1] = x[i] - funct(x[i])/first_derivative(x[i]);
  } while(!(abs(x[i+1] - x[i]) < eps));
  cout << x[i+1] << endl;
}



int main()
{
  double x1[MAX_SIZE], x2[MAX_SIZE];
  double eps;
  std::cin >> eps;
  x1[0] = -0.3;
  cout << "x1 = ";
  metod_Newtone(x1, eps);
  x2[0] = 20.8;
  cout << "x2 = ";
  metod_Newtone(x2, eps);

}
