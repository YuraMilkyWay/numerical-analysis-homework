#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
using namespace std;
const int MAX_SIZE = 50;
const double diff_eps = 0.1;

double f1(double x1, double x2)
{
    //return 0.1*pow(x1,2) + x1 + 0.2*pow(x2,2) - 0.3;
    //return x2*(pow(x1, 2) + 9) - 27;
    return pow(x1, 2) + pow(x2, 2) - 4;
}

double f2(double x1, double x2)
{
    //return 0.2*pow(x1,2) + x2 - 0.1*x1*x2 - 0.7;
    //return pow(x1,2) - 3*x1 + 9/4 + pow(x2,2) - 3*x2 + 9/4 - 9;
    //return pow((x1 - 3.0/2), 2) + pow((x2 - 3.0/2), 2) - 9;
    return x1 - pow(2.7, x2) + 2;
}

double diff_f1_x1(double x1, double x2)
{
    //return 0.2*x1 + 1;
    //return 2*x1;
    //return 2*x1*x2;
    return 2*x1;
}

double diff_f1_x2(double x1, double x2)
{
    //return 0.4*x2;
    //return pow(x1,2) + 9;
    //return pow(x1, 2) + 9;
    return 2*x2;
}

double diff_f2_x1(double x1, double x2)
{
    //return 0.4*x1 - 0.1*x2;
    //return 2*x1 - 3;
    //return 2*(x1 - 3.0/2);
    return 1;
}

double diff_f2_x2(double x1, double x2)
{
    //return 1 - 0.1*x1;
    //return 2*x2 - 3;
    //return 2*(x2 - 3.0/2);
    return -pow(2.7, x2);
}

double matrix_A1(double M[2][2], double x1, double x2)
{
  M[0][0] = f1(x1,x2);
  M[0][1] = diff_f1_x2(x1,x2);
  M[1][0] = f2(x1,x2);
  M[1][1] = diff_f2_x2(x1,x2);
  return M[0][0]*M[1][1] - M[1][0]*M[0][1];
}

double matrix_A2(double M[2][2], double x1, double x2)
{
  M[0][0] = diff_f1_x1(x1,x2);
  M[0][1] = f1(x1,x2);
  M[1][0] = diff_f2_x1(x1,x2);
  M[1][1] = f2(x1,x2);
  return M[0][0]*M[1][1] - M[1][0]*M[0][1];
}

double matrix_J(double M[2][2], double x1, double x2)
{
  M[0][0] = diff_f1_x1(x1,x2);
  M[0][1] = diff_f1_x2(x1,x2);
  M[1][0] = diff_f2_x1(x1,x2);
  M[1][1] = diff_f2_x2(x1,x2);
  return M[0][0]*M[1][1] - M[1][0]*M[0][1];
}

bool check_eps(double x1[MAX_SIZE], double x2[MAX_SIZE], int k, double eps)
{
  double abs1 = abs(x1[k+1]-x1[k]), abs2 = abs(x2[k+1]-x2[k]);
  return !((abs1 > abs2) ? (abs1 < eps) : (abs2 < eps));
}


double metod_Newtone(double x1[MAX_SIZE], double x2[MAX_SIZE], double eps)
{
  double A1[MAX_SIZE][2][2],
         A2[MAX_SIZE][2][2],
         J[MAX_SIZE][2][2];
  int i = 0;

  do {
    x1[i+1] = x1[i] - matrix_A1(A1[i], x1[i], x2[i])/matrix_J(J[i], x1[i],x2[i]);
    x2[i+1] = x2[i] - matrix_A2(A1[i], x1[i], x2[i])/matrix_J(J[i], x1[i],x2[i]);

    cout << "iteration № " << i+1 << endl;

    cout << "x1 = " << x1[i] << endl;
    cout << "x2 = " << x2[i] << endl;

    cout << "f1 = " << f1(x1[i], x2[i]) << endl;
    cout << "f2 = " << f2(x1[i], x2[i]) << endl;

    cout << "diff_f1_x1 = " << diff_f1_x1(x1[i],x2[i]) << endl;
    cout << "diff_f1_x2 = " << diff_f1_x2(x1[i],x2[i]) << endl;
    cout << "diff_f2_x1 = " << diff_f2_x1(x1[i],x2[i]) << endl;
    cout << "diff_f2_x2 = " << diff_f2_x2(x1[i],x2[i]) << endl;

    cout << "detA1 = " << matrix_A1(A1[i], x1[i], x2[i]) << endl;
    cout << "detA2 = " << matrix_A2(A1[i], x1[i], x2[i]) << endl;
    cout << "detJ = " << matrix_J(J[i], x1[i],x2[i]) << endl << endl;

    i++;
  } while(check_eps(x1, x2, i-1, eps));
  cout << "x1 = " << x1[i] << " x2 = " << x2[i] << endl;
}

int main()
{
  double x1[MAX_SIZE], x2[MAX_SIZE];
  double eps;
  cout << "enter the number eps = ";
  std::cin >> eps;

  x1[0] = 2;
  x2[0] = 1;
  metod_Newtone(x1, x2, eps);

}
