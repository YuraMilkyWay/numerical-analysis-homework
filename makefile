all: LU seidel iterat jacobi metod_Newtone_for_NLE simple_iteration_for_NLE metod_Newtone_for_SLAU simple_iteration_for_SNLE Interpolation_polynomials Cubic_spline metod_Gaussa system_mnk differentiation integral lab41 method_of_shooting method_finite_difference

LU: matrix.o LU.o
	g++-5 matrix.o LU.o -o LU

method_finite_difference: matrix.o method_finite_difference.o
	g++-5 matrix.o method_finite_difference.o -o method_finite_difference

seidel: matrix.o seidel.o
	g++-5 matrix.o seidel.o -o seidel

iterat: matrix.o iterat.o
	g++-5 matrix.o iterat.o -o iterat

jacobi: matrix.o jacobi.o
	g++-5 matrix.o jacobi.o -o jacobi

lab41: matrix.o lab41.o
	g++-5 matrix.o lab41.o -o lab41

metod_Gaussa: metod_Gaussa.o
	g++-5 metod_Gaussa.o -o lib/metod_Gaussa

integral: matrix.o integral.o
	g++-5 matrix.o integral.o -o integral

Cubic_spline: matrix.o metod_Gaussa.o Cubic_spline.o
	g++-5 matrix.o metod_Gaussa.o Cubic_spline.o -o Cubic_spline

method_of_shooting: matrix.o metod_Gaussa.o method_of_shooting.o
	g++-5 matrix.o metod_Gaussa.o method_of_shooting.o -o method_of_shooting

system_mnk: matrix.o metod_Gaussa.o system_mnk.o
	g++-5 matrix.o metod_Gaussa.o system_mnk.o -o system_mnk

metod_Newtone_for_NLE: metod_Newtone_for_NLE.o
	g++-5 metod_Newtone_for_NLE.o -o metod_Newtone_for_NLE

simple_iteration_for_NLE: simple_iteration_for_NLE.o
	g++-5 simple_iteration_for_NLE.o -o simple_iteration_for_NLE

metod_Newtone_for_SLAU: metod_Newtone_for_SLAU.o
	g++-5 metod_Newtone_for_SLAU.o -o metod_Newtone_for_SLAU

simple_iteration_for_SNLE: simple_iteration_for_SNLE.o
	g++-5 simple_iteration_for_SNLE.o -o simple_iteration_for_SNLE

Interpolation_polynomials: Interpolation_polynomials.o
	g++-5 Interpolation_polynomials.o -o Interpolation_polynomials

differentiation: differentiation.o
	g++-5 differentiation.o -o differentiation

simple_iterations_method: matrix.o simple_iterations_method.o
	g++-5 matrix.o simple_iterations_method.o -o simple_iterations_method


LU.o: LU.cpp
	g++-5 -c LU.cpp

method_finite_difference.o: method_finite_difference.cpp
	g++-5 -c method_finite_difference.cpp

integral.o: integral.cpp
	g++-5 -c integral.cpp

lab41.o: lab41.cpp
	g++-5 -c lab41.cpp

seidel.o: seidel.cpp
	g++-5 -c seidel.cpp

iterat.o: iterat.cpp
	g++-5 -c iterat.cpp

jacobi.o: jacobi.cpp
	g++-5 -c jacobi.cpp

Cubic_spline.o: Cubic_spline.cpp
	g++-5 -c Cubic_spline.cpp

method_of_shooting.o: method_of_shooting.cpp
	g++-5 -c method_of_shooting.cpp

system_mnk.o: system_mnk.cpp
	g++-5 -c system_mnk.cpp

matrix.o: lib/matrix.cpp
	g++-5 -c lib/matrix.cpp

metod_Gaussa.o: lib/metod_Gaussa.cpp
	g++-5 -c lib/metod_Gaussa.cpp

metod_Newtone_for_NLE.o: metod_Newtone_for_NLE.cpp
	g++-5 -c metod_Newtone_for_NLE.cpp

simple_iteration_for_NLE.o: simple_iteration_for_NLE.cpp
	g++-5 -c simple_iteration_for_NLE.cpp

metod_Newtone_for_SLAU.o: metod_Newtone_for_SLAU.cpp
	g++-5 -c metod_Newtone_for_SLAU.cpp

simple_iteration_for_SNLE.o: simple_iteration_for_SNLE.cpp
	g++-5 -c simple_iteration_for_SNLE.cpp

Interpolation_polynomials.o: Interpolation_polynomials.cpp
	g++-5 -c Interpolation_polynomials.cpp

differentiation.o: differentiation.cpp
	g++-5 -c differentiation.cpp

simple_iterations_method.o: simple_iterations_method.cpp
	g++-5 -c simple_iterations_method.cpp


clean:
	rm -rf *.o LU seidel test LU iterat jacobi metod_Newtone_for_NLE simple_iteration_for_NLE metod_Newtone_for_SLAU simple_iteration_for_SNLE Interpolation_polynomials Cubic_spline metod_Gaussa system_mnk differentiation integral lab41 simple_iterations_method method_of_shooting method_finite_difference
