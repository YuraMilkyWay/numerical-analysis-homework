#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include "lib/matrix.hpp"
using namespace std;
//const int MAX_SIZE = 50;
const double diff_eps = 0.1;
const double pi = 3.14159265;
typedef double (*f_pointer)(double x, double y, double z);

void interval(double x[MAX_SIZE], int n, double h)
{
  int i;
  for (i = 0; i < n; i++)
    x[i+1] = x[i] + h;
}

double p_x(double x)
{
  return -x;
}

double q_x(double x)
{
  return -1;
}

double first_array_y(double y[MAX_SIZE], int i, double h)
{
  return (y[i+1] - y[i-1])/(2*h);
}

double second_array_y(double y[MAX_SIZE], int i, double h)
{
  return (y[i+1] - 2*y[i] + y[i-1])/(pow(h,2));
}

double fun(double x[MAX_SIZE], double y[MAX_SIZE], int i, double h)
{
  return second_array_y(y, i, h) + p_x(x[i])*first_array_y(y, i, h) + q_x(x[i])*y[i];
}

double genial_system(double x[MAX_SIZE], double y[MAX_SIZE],
                     double g_s[MAX_SIZE][MAX_SIZE], int n, double h)
{
  for(int i = 1; i < n-1; i++)
  {
  //   g_s[i][i-1] = (1 - p_x(x[i]*h)/2);
  //   g_s[i][i] = (-2 + pow(h,2)*q_x(x[i]));
  //   g_s[i][i+1] = (1 + p_x(x[i])*h/2);
  //   g_s[i][n-1] = pow(h,2)*fun(x, y, i, h);
  // }
  // g_s[1][0] = (-2 + pow(h,2)*q_x(x[0]));
  // g_s[1][1] = (1 + p_x(x[0])*h/2);
  // g_s[1][5] = pow(h,2)*fun(x, y, 0, h) - (1 - (p_x(x[0])*h/2));
  //
  // g_s[0][0] = 1;
  // g_s[0][n-1] = 1;
  //
  // g_s[4][3] = (1 - p_x(x[4])*h/2);
  // g_s[4][4] = (-2 + pow(h,2)*q_x(x[4]));
  // g_s[4][5] = pow(h,2)*fun(x, y, 5, h) - (1 + p_x(5)*h/2);
    g_s[i][i-1] = 1.0/pow(h,2) + x[i]/(h*2);
    g_s[i][i] = (-2/pow(h,2)) - 1;
    g_s[i][i+1] = ((1/pow(h,2)) - x[i]/(2*h));
  }
  g_s[0][0] = 1;
  g_s[0][n] = 1;

  g_s[n-1][n-2] = -1.0/h;
  g_s[n-1][n-1] = (1.0/h) + 2;
}

int main()
{
  double x[MAX_SIZE], y[MAX_SIZE], z[MAX_SIZE],
         delta_y[MAX_SIZE], delta_z[MAX_SIZE],
         g_s[MAX_SIZE][MAX_SIZE], l[MAX_SIZE][MAX_SIZE],
         b = 1.0, h = 0.2;
  x[0] = 0, y[0] = 1;
  int n = 6;
  interval(x, n, h);
  print_vector_to_file(x, cout, n);
  genial_system(x, y, g_s, n, h);
  print_matrix_to_file(g_s, cout, n, n+1);

}
