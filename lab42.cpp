#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include "lib/metod_Gaussa.hpp"
#include "lib/matrix.hpp"
using namespace std;
//const int MAX_SIZE = 50;
const double eps = 0.0001;

int interval(double x[MAX_SIZE], double b, double h)
{
  int i;
  for (i = 0; x[i] <= b; i++)
    x[i+1] = x[i] + h;

  return i;
}

double funct_y(double x, double y, double z)
{
  return z;
}

double funct_z(double x, double y, double z)
{
  return exp(x) + sin(y);
}

double analytical_solution(double x, double y, double z)
{
  return pow(x,3) + 3*x + 1;
}

double search_phi(double b, double y, double eta[MAX_SIZE], int i)
{
  return funct_z(b, y, eta[i]) - funct_z(b, y, eta[1]);
}

double search_eta(double b, double y[MAX_SIZE], double eta[MAX_SIZE])
{
  int i = 1;
  do {
    i++;
    eta[i] = eta[i-1] - (eta[i-1] - eta[i-2])*(eta[i-1] - 2.0)/(eta[i-1] - eta[i-2]);
    cout << "eta = " << eta[i] << endl;
  } while(search_phi(b, y[0], eta, i) < eps);
  return i;
}

int main()
{
  double x[MAX_SIZE], y[MAX_SIZE], z[MAX_SIZE],
         delta_y[MAX_SIZE], delta_z[MAX_SIZE],
         eta[MAX_SIZE],
         k[MAX_SIZE][MAX_SIZE], l[MAX_SIZE][MAX_SIZE],
         b = 1.0, h = 0.1;
 int i = 2;
 y[0] = 1, eta[0] = 1, eta[1] = 0.8;
 cout << "funct_z(b, y[0], eta[0]) = " << funct_z(b, eta[0], eta[0]) << endl;
 cout << "funct_z(b, y[0], eta[1]) = " << funct_z(b, eta[1], eta[1]) << endl;
 // i = search_eta(b, y, eta);
 // print_vector_to_file(eta, cout, i);

}
