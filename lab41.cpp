#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include "lib/matrix.hpp"
using namespace std;
//const int MAX_SIZE = 50;
const double diff_eps = 0.1;
const double pi = 3.14159265;
typedef double (*f_pointer)(double x, double y, double z);

int interval(double x[MAX_SIZE], double b, double h)
{
  int i;
  for (i = 0; x[i] <= b; i++)
    x[i+1] = x[i] + h;

  return i;
}

double f_y(double x, double y, double z)
{
  return z;
}

double f_z(double x, double y, double z)
{
  //return 2*x*z/(pow(x,2) + 1);
  return 2*cos(x) - y;
}

double analytical_solution(double x, double y, double z)
{
  //return pow(x,3) + 3*x + 1;
  return x*sin(x) + cos(x);
}

void method_eulera(f_pointer f_y, f_pointer f_z, double x[MAX_SIZE], double y[MAX_SIZE],
                  double z[MAX_SIZE], double h, int n)
{
  for(int i = 0; i < n; i++)
  {
    y[i+1] = y[i] + h*f_y(x[i], y[i], z[i]);
    z[i+1] = z[i] + h*f_z(x[i], y[i], z[i]);
  }
}

void method_runge_kutta(f_pointer f_y, f_pointer f_z,
                       double x[MAX_SIZE], double y[MAX_SIZE],
                       double z[MAX_SIZE], double delta_y[MAX_SIZE],
                       double delta_z[MAX_SIZE], double k[MAX_SIZE][MAX_SIZE],
                       double l[MAX_SIZE][MAX_SIZE], double h, int n)
{
  for(int i = 0; i < n; i++)
  {
    for(int j = 0; j < 4; j++)
    {
      if (j == 0)
      {
        k[i][j] = h*f_y(x[i], y[i], z[i]);
        l[i][j] = h*f_z(x[i], y[i], z[i]);
      }
      else if (j == 3)
      {
        k[i][j] = h*f_y(x[i] + h, y[i] + k[i][j-1], z[i] + l[i][j-1]);
        l[i][j] = h*f_z(x[i] + h, y[i] + k[i][j-1], z[i] + l[i][j-1]);
      }
      else
      {
        k[i][j] = h*f_y(x[i] + (1.0/2)*h, y[i] + (1.0/2)*k[i][j-1], z[i] + (1.0/2)*l[i][j-1]);
        l[i][j] = h*f_z(x[i] + (1.0/2)*h, y[i] + (1.0/2)*k[i][j-1], z[i] + (1.0/2)*l[i][j-1]);
      }
    }

    delta_y[i] = (1.0/6)*(k[i][0] + 2*k[i][1] + 2*k[i][2] + k[i][3]);
    delta_z[i] = (1.0/6)*(l[i][0] + 2*l[i][1] + 2*l[i][2] + l[i][3]);
    y[i+1] = y[i] + delta_y[i];
    z[i+1] = z[i] + delta_z[i];
  }
}

void method_adamsa(f_pointer f_y, f_pointer f_z,
                  double x[MAX_SIZE], double y[MAX_SIZE],
                  double z[MAX_SIZE], double h, int n)
{
  for(int i = 3; i < n; i++)
  {
    y[i+1] = y[i] + (h/24)*(55*f_y(x[i], y[i], z[i]) - 59*f_y(x[i-1], y[i-1], z[i-1]) + 37*f_y(x[i-2], y[i-2], z[i-2]) - 9*f_y(x[i-3], y[i-3], z[i-3]));
    z[i+1] = z[i] + (h/24)*(55*f_z(x[i], y[i], z[i]) - 59*f_z(x[i-1], y[i-1], z[i-1]) + 37*f_z(x[i-2], y[i-2], z[i-2]) - 9*f_z(x[i-3], y[i-3], z[i-3]));
  }
}

void method_analitical(f_pointer analytical_solution,
                      double x[MAX_SIZE], double y[MAX_SIZE],
                      double z[MAX_SIZE], double h, int n)
{
  for(int i = 0; i < n; i++)
    y[i] = analytical_solution(x[i], y[i], z[i]);
}

void fun_print(double x[MAX_SIZE], double y[MAX_SIZE], int n)
   {
     for(int i = 0; i < n; i++)
        cout << "x = " << x[i] << " y = " << y[i] << endl;
   }

int main()
{
  double x[MAX_SIZE], y[MAX_SIZE], y_a[MAX_SIZE], z[MAX_SIZE],
         delta_y[MAX_SIZE], delta_z[MAX_SIZE],
         k[MAX_SIZE][MAX_SIZE], l[MAX_SIZE][MAX_SIZE],
         b = 1.0, h = 0.1;
  int n = interval(x, b, h);
  cout << endl << "n = " << n << endl;
  x[0] = 0.0, y[0] = 1, z[0] = 0;
  cout << "Metod Eulera" << endl;
  method_eulera(f_y, f_z, x, y, z, h, n);
  fun_print(x, y, n);

  cout << endl << "Metod Runge Kutta" << endl;
  method_runge_kutta(f_y, f_z, x, y, z, delta_y, delta_z, k, l, h, n);
  fun_print(x, y, n);

  cout << endl << "Metod Adamsa" << endl;
  method_adamsa(f_y, f_z, x, y, z, h, n);
  fun_print(x, y, n);

  cout << endl << "Analitical" << endl;
  method_analitical(analytical_solution, x, y_a, z, h, n);
  fun_print(x, y_a, n);

  cout << endl << "Error Runge Kutta - Analitical" << endl;
  for (int i = 0; i < n; i++)
    cout << "x = " << x[i] << " Error = " << y_a[i] - y[i] << endl;

}
