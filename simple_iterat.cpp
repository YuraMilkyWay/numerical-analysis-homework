#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
using namespace std;
const double eps = 0.1;
double A[MAX_SIZE][MAX_SIZE], X[MAX_SIZE];

void make_equivalent(double A[MAX_SIZE][MAX_SIZE], int n)
{
  for(int i = 0; i < n; i++)
  {
    if ((diviser = A[i][i]) == 0)
      continue;
    else
      for(int j = 0; j < n + 1; j++)
        A[i][j] = (i == j) ? 0 : (A[i][j] / diviser);
  }
}

int main()
{
  ifstream fin("input.txt");
  ofstream fout("output.txt");

  fout.precision(5);
  cout.precision(5);

  int n = input_matrix(A, fin);
  make_equivalent(A, n);
  print_matrix_to_file(A, cout, n, n+1);
  return 0;
}
