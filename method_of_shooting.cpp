#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include "lib/metod_Gaussa.hpp"
#include "lib/matrix.hpp"
using namespace std;
//const int MAX_SIZE = 50;
const double eps = 0.0001;

int interval(double x[MAX_SIZE], double b, double h)
{
  int i;
  for (i = 0; x[i] <= b; i++)
    x[i+1] = x[i] + h;

  return i;
}

double f_y(double x, double y, double z)
{
  return z;
}

double f_z(double x, double y, double z)
{
  return exp(x) + sin(y);
}

double analytical_solution(double x, double y, double z)
{
  return pow(x,3) + 3*x + 1;
}

void method_runge_kutta(double x[MAX_SIZE], double y[MAX_SIZE],
                        double z[MAX_SIZE], double delta_y[MAX_SIZE],
                        double delta_z[MAX_SIZE], double k[MAX_SIZE][MAX_SIZE],
                        double l[MAX_SIZE][MAX_SIZE], double h, int n)
{
  for(int i = 0; i < n; i++)
  {
    for(int j = 0; j < 4; j++)
    {
      if (j == 0)
      {
        k[i][j] = h*f_y(x[i], y[i], z[i]);
        l[i][j] = h*f_z(x[i], y[i], z[i]);
      }
      else if (j == 3)
      {
        k[i][j] = h*f_y(x[i] + h, y[i] + k[i][j-1], z[i] + l[i][j-1]);
        l[i][j] = h*f_z(x[i] + h, y[i] + k[i][j-1], z[i] + l[i][j-1]);
      }
      else
      {
        k[i][j] = h*f_y(x[i] + (1.0/2)*h, y[i] + (1.0/2)*k[i][j-1], z[i] + (1.0/2)*l[i][j-1]);
        l[i][j] = h*f_z(x[i] + (1.0/2)*h, y[i] + (1.0/2)*k[i][j-1], z[i] + (1.0/2)*l[i][j-1]);
      }
    }

    delta_y[i] = (1.0/6)*(k[i][0] + 2*k[i][1] + 2*k[i][2] + k[i][3]);
    delta_z[i] = (1.0/6)*(l[i][0] + 2*l[i][1] + 2*l[i][2] + l[i][3]);
    y[i+1] = y[i] + delta_y[i];
    z[i+1] = z[i] + delta_z[i];
  }
}

double search_eta(double x[MAX_SIZE], double y[MAX_SIZE][MAX_SIZE],
                  double z[MAX_SIZE][MAX_SIZE], double delta_y[MAX_SIZE],
                  double delta_z[MAX_SIZE], double k[MAX_SIZE][MAX_SIZE],
                  double l[MAX_SIZE][MAX_SIZE], double fedya[MAX_SIZE],
                  double h, int n)
{

  int i = -1;
  do {
    i++;
    y[i][0] = 1;
    if ( i < 2)
      method_runge_kutta(x, y[i], z[i], delta_y, delta_z, k, l, h, n);
    else
    {
      z[i][0] = z[i-1][0] - (z[i-1][0] - z[i-2][0])*(y[i-1][n-1] - 2)/(y[i-1][n-1] - y[i-2][n-1]);
      method_runge_kutta(x, y[i], z[i], delta_y, delta_z, k, l, h, n);
    }
    fedya[i] = abs(y[i][n-1] - 2);
  } while(fedya[i] > eps);
  return i;
}

void print_qq(int j, double z[MAX_SIZE][MAX_SIZE], double y[MAX_SIZE][MAX_SIZE],
              double fedya[MAX_SIZE], double x[MAX_SIZE], int n)
  {

    for(int i = 0; i <= j; i++)
    {
      cout << "j = " << i << " n[j] = " << z[i][0] << " y(1, 1, n[j]) = " << y[i][n-1] << " |F(n[j])| = " << fedya[j] << endl;
    }
    cout << endl;

    for(int i = 0; i < n; i++)
    {
      cout << fixed << "x[k] = " << x[i] << " y[k] = " << y[j][i] << endl;
    }
  }

int main()
{
  double x[MAX_SIZE], y[MAX_SIZE][MAX_SIZE], z[MAX_SIZE][MAX_SIZE],
         delta_y[MAX_SIZE], delta_z[MAX_SIZE],
         eta[MAX_SIZE][MAX_SIZE],
         k[MAX_SIZE][MAX_SIZE], l[MAX_SIZE][MAX_SIZE],
         fedya[MAX_SIZE],
         b = 1.0, h = 0.1;
 y[0][0] = y[1][0] = 1, z[0][0] = 1, z[1][0] = 0.8;
 cout.precision(4);
 int n = interval(x, b, h);
 int j = search_eta(x, y, z, delta_y, delta_z, k, l, fedya, h, n);
 print_qq(j, z, y, fedya, x, n);

}
