#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
using namespace std;
const int MAX_SIZE = 50;
const double diff_eps = 0.1;
const double pi = 3.14159265;

double y(double x)
{
    //return 0.3 - 0.1*pow(x1,2) - 0.2*pow(x2,2);
    //return log(x);
    return cos(x);
}

double multi_w(int i, double x[4], double X)
{
 double w = 1, w4;
 w4 = (X - x[0])*(X - x[1])*(X - x[2])*(X - x[3]);

 for(int j = 0; j < 4; j++ )
  {
    if ( j == i)
      continue;
    else
      w *= (x[i] - x[j]);
      //cout << "w = " << w << endl;
      //cout << "(x[i] - x[j]) " << (x[i] - x[j]) << endl;


  }
  //cout << "w = " << w << endl;
  return w4/((X - x[i])*w);
}

void metod_Lagrange(double x[4], double X)
{
  double L = 0;

  for (int i = 0; i < 4; i++)
    L += y(x[i])*multi_w(i,x,X);

  cout << "L = " << L << endl;
  cout << "y(pi/4) = " << y(X) << endl;
  cout << "delta(L(pi/4)) = " << abs(L - y(X)) << endl << endl;
}

double f_2x(double xi, double xj)
{
  return (y(xi) - y(xj))/(xi - xj);
}

double f_3x(double xi, double xj, double xk)
{
  return (f_2x(xi, xj) - f_2x(xj, xk))/(xi - xk);
}

double f_4x(double xi, double xj, double xk, double xh)
{
  return (f_3x(xi, xj, xk) - f_3x(xj, xk, xh))/(xi - xh);
}

double sub_x(double X, double x[4], int n)
{
  double multiplication = 1;
  for(int i = 0; i < n; i++)
    multiplication *= X - x[i];

  return multiplication;
}

double metod_Newtone(double x[4], double X)
{
  double P = y(x[0]) + sub_x(X,x,1)*f_2x(x[1],x[0]) + sub_x(X,x,2)*f_3x(x[0],x[1],x[2]) + sub_x(X,x,3)*f_4x(x[0],x[1],x[2],x[3]);
  cout << "P = " << P << endl;
  cout << "y(pi/4) = " << y(X) << endl;
  cout << "delta(P(pi/4)) = " << abs(P - y(X)) << endl;
}

int main()
{
  double x1[4], x2[4], X1 = pi/4, X2 = pi/4;

  x1[0] = 0, x1[1] = pi/6, x1[2] = 2*pi/6, x1[3] = 3*pi/6;
  x2[0] = 0, x2[1] = pi/6, x2[2] = 5*pi/12, x2[3] = pi/2;
  // x1[0] = 0.1, x1[1] = 0.5, x1[2] = 0.9, x1[3] = 1.3;
  // x2[0] = 0, x2[1] = 1, x2[2] = 2, x2[3] = 3;

  metod_Lagrange(x1, X1);
  metod_Newtone(x2, X2);


}
