#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include "lib/matrix.hpp"
#include "lib/metod_Gaussa.hpp"
using namespace std;
//const int MAX_SIZE = 50;
const double diff_eps = 0.1;
const double pi = 3.14159265;

double sum_ext(double m[MAX_SIZE], int power, int n)
{
  double temp = 0;
  for (int i = 0; i <= n; i++)
    temp += pow(m[i], power);
  return temp;
}

double sum_multi_ext(double first_m[MAX_SIZE], int first_power,
                     double second_m[MAX_SIZE], int second_power,
                     int n)
{
  double temp = 0;
  for (int i = 0; i <= n; i++)
    temp += pow(first_m[i], first_power)*pow(second_m[i], second_power);
  return temp;
}

void first_system_mnk(double matrix_a[MAX_SIZE][MAX_SIZE], double x[MAX_SIZE],
                      double y[MAX_SIZE], int n)
{
  matrix_a[0][0] = n;
  matrix_a[0][1] = sum_ext(x, 1, n);
  matrix_a[0][2] = sum_ext(y, 1, n);
  matrix_a[1][0] = sum_ext(x, 1, n);
  matrix_a[1][1] = sum_ext(x, 2, n);
  matrix_a[1][2] = sum_multi_ext(x, 1, y, 1, n);
  print_matrix_to_file(matrix_a, cout, 2, 3);
}

void first_polynominal(double matrix_a[MAX_SIZE][MAX_SIZE], double x[MAX_SIZE],
                      double y[MAX_SIZE], double a[MAX_SIZE], double f1_x[MAX_SIZE],
                      int n)
{
  first_system_mnk(matrix_a, x, y, n);
  solution_of_the_system(matrix_a, 2, a);
  for (int i = 0; i < n; i++)
    f1_x[i] = a[0] + a[1]*x[i];
}

double sum_of_error_squares(double f1_x[MAX_SIZE], double y[MAX_SIZE],
                            double error_f1[MAX_SIZE], int n)
{
  for (int i = 0; i < n; i++)
    error_f1[i] = f1_x[i] - y[i];
  return sum_ext(error_f1, 2, n);
}


             /* SEEEEEEEEEEEEEECOND POOOOOOOOOOOLYNOMINAL*/

void second_system_mnk(double matrix_a[MAX_SIZE][MAX_SIZE], double x[MAX_SIZE],
                     double y[MAX_SIZE], int n)
{
  matrix_a[0][0] = n;
  matrix_a[0][1] = sum_ext(x, 1, n);
  matrix_a[0][2] = sum_ext(x, 2, n);
  matrix_a[0][3] = sum_ext(y, 1, n);
  matrix_a[1][0] = sum_ext(x, 1, n);
  matrix_a[1][1] = sum_ext(x, 2, n);
  matrix_a[1][2] = sum_ext(x, 3, n);
  matrix_a[1][3] = sum_multi_ext(x, 1, y, 1, n);
  matrix_a[1][1] = sum_ext(x, 2, n);
  matrix_a[2][0] = sum_ext(x, 2, n);
  matrix_a[2][1] = sum_ext(x, 3, n);
  matrix_a[2][2] = sum_ext(x, 4, n);
  matrix_a[2][3] = sum_multi_ext(x, 2, y, 1, n);

  print_matrix_to_file(matrix_a, cout, 3, 4);
}


void second_polynominal(double matrix_a[MAX_SIZE][MAX_SIZE], double x[MAX_SIZE],
                      double y[MAX_SIZE], double a[MAX_SIZE],
                      double f2_x[MAX_SIZE], int n)
{
  second_system_mnk(matrix_a, x, y, n);
  solution_of_the_system(matrix_a, 3, a);
  for (int i = 0; i < n; i++)
    f2_x[i] = a[0] + a[1]*x[i] + a[2]*pow(x[i], 2);
}


int main()
{
  double matrix_a1[MAX_SIZE][MAX_SIZE], matrix_a2[MAX_SIZE][MAX_SIZE], a[MAX_SIZE],
  x[MAX_SIZE], y[MAX_SIZE], f1_x[MAX_SIZE], error_f1[MAX_SIZE], f2_x[MAX_SIZE], error_f2[MAX_SIZE];
  int n = 6;

  // x[0] = 0.0,  x[1] = 1.7,  x[2] = 3.4,  x[3] = 5.1,  x[4] = 6.8,  x[5] = 8.5;
  // y[0] = 0.0,  y[1] = 1.3038,  y[2] = 1.8439,  y[3] = 2.2583,  y[4] = 2.6077,  y[5] = 2.9155;
  // x[0] = -1.0,  x[1] = 0.0,  x[2] = 1.0,  x[3] = 2.0,  x[4] = 3.0,  x[5] = 4.0;
  // y[0] = 0.86603,  y[1] = 1.0,  y[2] = 0.86603,  y[3] = 0.50,  y[4] = 0.0,  y[5] = -0.5;

  x[0] = 0.0,  x[1] = 0.2,  x[2] = 0.4,  x[3] = 0.6,  x[4] = 0.8,  x[5] = 1.0;
  y[0] = 1,  y[1] = 1.0032,  y[2] = 1.0512,  y[3] = 1.2592,  y[4] = 1.8192,  y[5] = 3;

  cout << endl << " First mnk system ";
  first_polynominal(matrix_a1, x, y, a, f1_x, n);
  print_vector_to_file(f1_x, cout, 6, "f1_x ");
  cout << " sum_of_error_squares = " << sum_of_error_squares(f1_x, y, error_f1, n) << endl;

  cout  << endl << " Second mnk system ";
  second_polynominal(matrix_a2, x, y, a, f2_x, n);
  print_vector_to_file(f2_x, cout, 6);
  cout << " sum_of_error_squares = " << sum_of_error_squares(f2_x, y, error_f2, n) << endl;
}
