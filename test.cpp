#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include "lib/matrix.hpp"
#include <cmath>
using namespace std;

string concat_string_with_int(string s, int i)
{
	s.push_back(i + '0');
	return s;
}

double max_double_array_element(double *a, int n)
{
	double max = a[0];
	for(int i = 1; i < n; i++)
		if (a[i] > max)
			max = a[i];
	return max;
}

double max_norm(double *XK, double *XKP, int n)
{
	double norm[n];

	for (int i = 0; i < n; i++)
	{
		norm[i] = (XK[i] - XKP[i])*(XK[i] - XKP[i]);
		norm[i] = sqrt(norm[i]);
	}

	return max_double_array_element(norm, n);

}

bool converge(double *XK, double *XKP, int n, int k, double eps, std::ostream &fout)
{
	double max_n = max_norm(XK, XKP, n);
	fout << "Iteration № " << k << " eps: " << max_n << endl;
	if (max_n >= eps)
			return false;
	return true;
}

void seidel_method(double A[MAX_SIZE][MAX_SIZE],
                   double B[MAX_SIZE],
		               int n, double eps, std::ostream &fout)
{
		double X[MAX_SIZE], P[MAX_SIZE], var;
		int i, j, k = 0;

		for(i = 0; i < n; i++)
			X[i] = B[i] = B[i]/A[i][i];

		do
		{
			for(i = 0; i < n; i++)
	      P[i] = X[i];

			for (i = 0; i < n; i++)
			{
					var = 0;
          for (j = 0; j < n; j++)
					{
						if (i != j)
							var += -A[i][j]*P[j]/A[i][i];
					}
					X[i] = var + B[i];
      }
			print_vector_to_file(X, fout, n, concat_string_with_int("Value X № ", ++k));
		} while (!converge(X, P, n, k, eps, fout));
}



int main()
{
	double A[MAX_SIZE][MAX_SIZE], L[MAX_SIZE][MAX_SIZE],
         U[MAX_SIZE][MAX_SIZE], R[MAX_SIZE][MAX_SIZE],
         B[MAX_SIZE], X[MAX_SIZE], eps = 0.01;
	int i = 0;

 	ifstream fin("input.txt");
	ofstream fout("output.txt");

	fout.precision(5);
	cout.precision(5);
	int n = input_conjugated_matrix(A, B, fin);


	seidel_method(A, B, n, eps, fout);
	//
	// LU(A,L,U,n);
	// multiply_matrices(L, U, R, n);
	// print_matrix_to_file(U, fout, n, n, "U matrix");
	// print_matrix_to_file(L, fout, n, n, "L matrix");
	// print_matrix_to_file(R, fout, n, n, "L*U matrix");

	fin.close();
	fout.close();
	return 0;
}
