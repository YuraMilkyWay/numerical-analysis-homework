#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include "lib/matrix.hpp"
#include <cmath>
using namespace std;

void copy_2D_matrixes(double A[MAX_SIZE][MAX_SIZE],
											double B[MAX_SIZE][MAX_SIZE], int n)
{
	for (int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			B[i][j] = A[i][j];
}

void max_element(double A[MAX_SIZE][MAX_SIZE], int n, int *k, int *m)
{
	double t = 0;
	for (int i = 0; i < n; i++)
		for (int j = i+1; j < n; j++)
			{
				if ( abs(A[i][j]) >= t)
				{
					t = abs(A[i][j]);
					*k = i;
					*m = j;
				}
			}

//	cout << endl << A[*k][*m] << endl;
}

double angle_of_rotation(double A[MAX_SIZE][MAX_SIZE], int k, int m)
{
	double a = 2*A[k][m]/(A[k][k] - A[m][m]);
	return 0.5*atan(a);
}

void matrix_of_rotation(double H[MAX_SIZE][MAX_SIZE],
													int n, int k, int m, double phi)
{
	for (int i = 0; i < n; i++)
		H[i][i] = 1;

	H[k][k] = cos(phi);
	H[k][m] = -sin(phi);
	H[m][k] = sin(phi);
	H[m][m] = cos(phi);
}

void matrix_of_rotated(double A[MAX_SIZE][MAX_SIZE],
											 double H[MAX_SIZE][MAX_SIZE],
											 double HT_A_H[MAX_SIZE][MAX_SIZE],
										 	 int n)
{
	double HT[MAX_SIZE][MAX_SIZE], HT_A[MAX_SIZE][MAX_SIZE];

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			HT[i][j] = H[j][i];

	multiply_matrices(HT, A, HT_A, n);
	multiply_matrices(HT_A, H, HT_A_H, n);
}

bool check_eps(double A[MAX_SIZE][MAX_SIZE], int n, double eps)
{
	print_matrix_to_file(A, cout, n, n);
	double e = 0;
	for (int i = 0; i < n; i++)
		for (int j = i+1; j < n; j++)
				e += A[i][j]*A[i][j];
	// cout << endl << "e = " << e << " eps: " << eps << endl;
	return !(e >= eps);
}

void Eigenvalues(double A[MAX_SIZE][MAX_SIZE], int n)
{
	for (int i = 0; i < n; i++)
		print_element_to_file(A[i][i], cout, n, i);
}

void Eigenvectors(double rotation[MAX_SIZE][MAX_SIZE][MAX_SIZE], int n, int i)
{
	double X[MAX_SIZE], sum_H[MAX_SIZE][MAX_SIZE], H[MAX_SIZE][MAX_SIZE];

	copy_2D_matrixes(rotation[0], sum_H, n);
	for (int j = 1; j < i; j++)
		{
			multiply_matrices(sum_H, rotation[j], H, n);
			copy_2D_matrixes(H, sum_H, n);
		}
	cout << endl;
	print_matrix_to_file(sum_H, cout, n, n, "sum_H = ");
	print_vectors_from_matrix(sum_H, cout, n);
}



void jacobi(double A[MAX_SIZE][MAX_SIZE], int n, double eps)
{
	double rotated[MAX_SIZE][MAX_SIZE][MAX_SIZE],
	rotation[MAX_SIZE][MAX_SIZE][MAX_SIZE];
	int k, m, i = 0;
	copy_2D_matrixes(A, rotated[0], n);

	//for (int i = 0; i < 7; i++)
	do
	{
		max_element(rotated[i], n, &k, &m);
		double phi = angle_of_rotation(rotated[i], k, m);
		// cout << phi << endl;
		matrix_of_rotation(rotation[i], n, k, m, phi);
		matrix_of_rotated(rotated[i], rotation[i], rotated[i+1], n);
		// print_matrix_to_file(rotation[i], cout, n, n);
		// print_matrix_to_file(rotated[i+1], cout, n, n);
		// cout << endl << "i = " << i << endl;
		i++;
	} while (!check_eps(rotated[i], n, eps));

	Eigenvalues(rotated[i], n);
	Eigenvectors(rotation, n, i);

}




int main()
{
	double A[MAX_SIZE][MAX_SIZE], L[MAX_SIZE][MAX_SIZE],
         U[MAX_SIZE][MAX_SIZE], R[MAX_SIZE][MAX_SIZE],
         B[MAX_SIZE], X[MAX_SIZE], eps = 0.3;
	int i = 0;

 	ifstream fin("input.txt");
	ofstream fout("output.txt");

	fout.precision(5);
	cout.precision(5);

	int n = input_matrix(A, fin);
	jacobi(A, n, eps);

	fin.close();
	fout.close();
	return 0;
}
