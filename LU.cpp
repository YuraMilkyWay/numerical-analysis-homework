#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include "lib/matrix.hpp"
using namespace std;

void LU(double A[MAX_SIZE][MAX_SIZE], double L[MAX_SIZE][MAX_SIZE],
		double U[MAX_SIZE][MAX_SIZE], int n)
{
	//U=A
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			U[i][j]=A[i][j];

	for(int k = 1; k < n; k++)
	{
		for(int i = k-1; i < n; i++)
			for(int j = i; j < n; j++)
				L[j][i]=U[j][i]/U[i][i];

		for(int i = k; i < n; i++)
			for(int j = k-1; j < n; j++)
				U[i][j]=U[i][j]-L[i][k-1]*U[k-1][j];
	}

}

int main()
{
	double A[MAX_SIZE][MAX_SIZE], L[MAX_SIZE][MAX_SIZE],
         U[MAX_SIZE][MAX_SIZE], R[MAX_SIZE][MAX_SIZE],
         B[MAX_SIZE];

 	ifstream fin("input.txt");
	ofstream fout("output.txt");
	int m = 1;
	fout.precision(5);
	int n = input_conjugated_matrix(A, B, fin);

	print_matrix_to_file(A, fout, n, n, "Input matrix_A");
	print_vector_to_file(B, fout, n, "Input vector_B");

	LU(A,L,U,n);
	multiply_matrices(L, U, R, n);
	//
	print_matrix_to_file(U, fout, n, n, "U matrix");
	print_matrix_to_file(L, fout, n, n, "L matrix");
	print_matrix_to_file(R, fout, n, n, "L*U matrix");

	fin.close();
	fout.close();
	return 0;
}
