#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include "matrix.hpp"
#include <cmath>

void multiply_matrices(double A[MAX_SIZE][MAX_SIZE],
                       double B[MAX_SIZE][MAX_SIZE],
			                 double R[MAX_SIZE][MAX_SIZE], int n)
{
  for(int i = 0; i < n; i++)
    for(int j = 0; j < n; j++)
      R[i][j] = 0;

	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			for(int k = 0; k < n; k++)
				R[i][j] += A[i][k] * B[k][j];
}

int multiply_matrixes(double A[MAX_SIZE][MAX_SIZE],
                      double B[MAX_SIZE][MAX_SIZE],
			                double R[MAX_SIZE][MAX_SIZE],
                      int r1, int c1, int c2, int r2)
{
  if (c1 != c2)
    return -1;

  for(int i = 0; i < r1; i++)
    for(int j = 0; j < c2; j++)
      R[i][j] = 0;

  for(int i = 0; i < r1; i++)
    for(int j = 0; j < c2; j++)
      for(int k = 0; k < c1; k++)
        R[i][j] += A[i][k] * B[k][j];
}

int add_matrixes(double A[MAX_SIZE][MAX_SIZE],
              	 double B[MAX_SIZE][MAX_SIZE],
                 double R[MAX_SIZE][MAX_SIZE],
								 int n, int m)
{
  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++)
      R[i][j] = A[i][j] + B[i][j];
}

int subtract_matrixes(double A[MAX_SIZE][MAX_SIZE],
              	      double B[MAX_SIZE][MAX_SIZE],
                      double R[MAX_SIZE][MAX_SIZE],
								      int n, int m)
{
  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++)
      R[i][j] = A[i][j] - B[i][j];
}

int copy_matrixes(double A[MAX_SIZE][MAX_SIZE],
              	  double B[MAX_SIZE][MAX_SIZE],
								  int n, int m)
{
  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++)
      A[i][j] = B[i][j];
}

int input_matrix(double A[MAX_SIZE][MAX_SIZE], std::istream &fin)
{

  int i = 0, j = 0;
  char b;

  while( !fin.eof() )
  {
    do
    {
      fin >> A[i][j++];
      fin.get(b);
    }
    while (b != '\n');
    i++;
    j=0;
  }

  return i-1;
}

int input_conjugated_matrix(double A[MAX_SIZE][MAX_SIZE],
														double B[MAX_SIZE], std::istream &fin)
{

  int i = 0, j = 0;
  char c;

  while( !fin.eof() )
  {
    do
    {
      fin >> A[i][j++];
      fin.get(c);
    }
    while (c != '\n');
		B[i] = A[i][j-1];
    i++;
    j=0;
  }



  return i-1;
}

void print_matrix_to_file(double A[MAX_SIZE][MAX_SIZE], std::ostream &fout, int n, int m, std::string comment)
{


	fout << comment << std::endl;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			fout << " " << std::setw(7) << std::showpoint << A[i][j] << " ";
		}
		fout << std::endl;
	}


}

void print_vector_to_file(double B[MAX_SIZE], std::ostream &fout, int n, std::string comment)
{

	fout << comment << std::endl;
	for(int i = 0; i < n; i++)
	{
		fout << " " << std::setw(7) << std::showpoint << B[i] << " ";
		fout << std::endl;
	}
}

void print_element_to_file(double e, std::ostream &fout, int n, int i, std::string comment)
{
  fout << comment << std::endl;
  fout << " lambda" << i << " = " << std::setw(7) << std::showpoint << e << " ";
  fout << std::endl;

}

void print_vectors_from_matrix(double A[MAX_SIZE][MAX_SIZE], std::ostream &fout, int n, std::string comment)
{
  fout << comment << std::endl;
  for (int j = 0; j < n; j++)
  {
    fout << "X" << j+1 << std::endl;
    for (int i = 0; i < n; i++)
    {
      fout << " " << std::setw(7) << std::showpoint << A[i][j] << " ";
  		fout << std::endl;
    }
  }

}
