#ifndef _matrix_hpp
#define _matrix_hpp
const int MAX_SIZE = 50;
const double ZERO_PRECISION = 0.000000001;

void multiply_matrices(double A[MAX_SIZE][MAX_SIZE], double B[MAX_SIZE][MAX_SIZE],
			                 double R[MAX_SIZE][MAX_SIZE], int n);
int multiply_matrixes(double A[MAX_SIZE][MAX_SIZE],
                    	double B[MAX_SIZE][MAX_SIZE],
			                double R[MAX_SIZE][MAX_SIZE],
											int r1, int c1, int c2, int r2);
int add_matrixes(double A[MAX_SIZE][MAX_SIZE],
              	 double B[MAX_SIZE][MAX_SIZE],
                 double R[MAX_SIZE][MAX_SIZE],
								 int n, int m);
int subtract_matrixes(double A[MAX_SIZE][MAX_SIZE],
              	 double B[MAX_SIZE][MAX_SIZE],
                 double R[MAX_SIZE][MAX_SIZE],
								 int n, int m);
int copy_matrixes(double A[MAX_SIZE][MAX_SIZE],
             	   double B[MAX_SIZE][MAX_SIZE],
								 int n, int m);
int input_matrix(double A[MAX_SIZE][MAX_SIZE], std::istream &fin);
void print_matrix_to_file(double A[MAX_SIZE][MAX_SIZE], std::ostream &fout, int n, int m, std::string comment = "");

void print_vector_to_file(double B[MAX_SIZE], std::ostream &fout, int n, std::string comment = "");
int input_conjugated_matrix(double A[MAX_SIZE][MAX_SIZE], double B[MAX_SIZE], std::istream &fin);

void print_vectors_from_matrix(double A[MAX_SIZE][MAX_SIZE], std::ostream &fout, int n, std::string comment = "");
void print_element_to_file(double e, std::ostream &fout, int n, int i, std::string comment = "");

#endif
