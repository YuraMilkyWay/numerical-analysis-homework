#ifndef _metod_Gaussa_hpp
#define _metod_Gaussa_hpp
const int MAX_BUDLO = 50;

void copy_mass(double a[MAX_BUDLO], double b[MAX_BUDLO], int l);
void permutation_of_strings(double a[MAX_BUDLO][MAX_BUDLO], int n, int c, int l);
void division(double a[MAX_BUDLO][MAX_BUDLO], int c, int n);
void deduct(double a[MAX_BUDLO][MAX_BUDLO], int u, int n);
void normalize_matrix(double a[MAX_BUDLO][MAX_BUDLO], int n);
void search_x(double a[MAX_BUDLO][MAX_BUDLO], double x[MAX_BUDLO], int n);
void solution_of_the_system(double a[MAX_BUDLO][MAX_BUDLO], int n, double x[MAX_BUDLO]);
double recr(double a[MAX_BUDLO][MAX_BUDLO], double x[MAX_BUDLO], int n);
#endif
