#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
const int MAX_BUDLO = 50;
using namespace std;

// void print_mat(double a[MAX_BUDLO][MAX_BUDLO], int n)
// {
//   for(int i = 0; i < n; i++)
//   {
//     for(int j = 0; j < n+1; j++)
//     {
//       cout << " " << std::setw(7) << std::showpoint << a[i][j] << " ";
//     }
//     cout << std::endl;
//   }
// }

void copy_mass(double a[MAX_BUDLO], double b[MAX_BUDLO], int l)
{
  double temp;
  for(int i = l; i < MAX_BUDLO; i++)
    {
      temp = a[i];
      a[i] = b[i];
      b[i] = temp;
    }
}

void permutation_of_strings(double a[MAX_BUDLO][MAX_BUDLO], int n, int c, int l)
{
  double temp[MAX_BUDLO];
  for (int j = c; j < n; j++)
    {
      if (a[j][l] < a[j+1][l])
        {
          copy_mass(a[j], a[j+1], 0);
          j = c - 1;
        }
      else
        continue;
    }
}

void division(double a[MAX_BUDLO][MAX_BUDLO], int c, int n)
{
  for (int j = c; j < n; j++)
    {
      double temp = a[j][c];
      if (temp == 0)
        continue;
      else
        for( int i = c; i < n+1; i++)
        {
          a[j][i] = a[j][i]/temp;
        }
    }
}

void deduct(double a[MAX_BUDLO][MAX_BUDLO], int u, int n)
{
  for (int i = u; i < n; i++)
    {
      if ( a[i+1][u] == 0)
        continue;

      else
        for (int j = u; j < n + 1; j++)
          a[i+1][j] = a[i+1][j] - a[u][j];
    }
}

void normalize_matrix(double a[MAX_BUDLO][MAX_BUDLO], int n)
{
  for( int c = 0; c < n; c++)
  {
    permutation_of_strings(a, n, 0, 0);
    // cout << " 1 " << endl;
    // print_mat(a, n);
    division(a, c, n);
    // cout << " 2 " << endl;
    // print_mat(a, n);
    deduct(a, c, n);

  }

}

void search_x(double a[MAX_BUDLO][MAX_BUDLO], double x[MAX_BUDLO], int n)
{
  x[2] = a[2][3];
  x[1] = a[1][3] - a[1][2]*x[2];
  x[0] = a[0][3] - a[0][1]*x[1] - a[0][2]*x[2];
}

double recr(double a[MAX_BUDLO][MAX_BUDLO], double x[MAX_BUDLO], int n)
{
  for (int i = n; i > -1; i--)
  {
    double multi = 0;
    for (int j = i + 1; j <= n; j++)
    {
      multi += a[i][j]*x[j];
    }
    x[i] = a[i][n] - multi;
  }
}


void solution_of_the_system(double a[MAX_BUDLO][MAX_BUDLO], int n, double x[MAX_BUDLO])
{
  normalize_matrix(a, n);
  //search_x(a, x, n);
  recr(a, x, n);
}

// int main()
// {
//   double a[MAX_BUDLO][MAX_BUDLO], x[MAX_BUDLO];
//   int n = 3;
//   a[0][0] = 4;
//   a[0][1] = 1;
//   a[0][2] = 0;
//   a[0][3] = -2.3209;
//   a[1][0] = 1;
//   a[1][1] = 4;
//   a[1][2] = 1;
//   a[1][3] = -2.5080;
//   a[2][0] = 0;
//   a[2][1] = 1;
//   a[2][2] = 4;
//   a[2][3] = -0.38924;
//   // a[0][0] = 1;
//   // a[0][1] = 2;
//   // a[0][2] = 0;
//   // a[0][3] = 5;
//   // a[1][0] = 0;
//   // a[1][1] = 1;
//   // a[1][2] = 5;
//   // a[1][3] = 10;
//   // a[2][0] = 0;
//   // a[2][1] = 0;
//   // a[2][2] = 1;
//   // a[2][3] = 100;
//   // print_mat(a, n);
//   // recr(a, x, n);
//
//   cout << endl;
//   Solution_of_the_system(a, n, x);
//   for (int i = 0; i < n; i++)
//     cout << x[i] << endl;

// }
