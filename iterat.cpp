#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include "lib/matrix.hpp"
#include <cmath>
using namespace std;

void forward_iteration(double A[MAX_SIZE][MAX_SIZE],
									 double B[MAX_SIZE],
									 double P[MAX_SIZE],
									 double Q[MAX_SIZE],
								 	 double n)
{
	double a[MAX_SIZE], c[MAX_SIZE], b[MAX_SIZE];
	int i, j;

	P[0] = Q[0] = 0;
	for ( i = 0; i < n; i++)
	{
		a[i] = (i == 0) ? 0 : A[i][i-1];
		b[i] = -A[i][i];
		c[i] = (i == n-1) ? 0 : A[i][i+1];

		P[i+1] = c[i]/(b[i] - a[i]*P[i]);
		Q[i+1] = (a[i]*Q[i] - B[i])/(b[i] - a[i]*P[i]);

	}
}

void reverse_iteration(double P[MAX_SIZE],
											 double Q[MAX_SIZE],
										 	 double X[MAX_SIZE],
										 	 int n)
{
	int i, j;

	X[n-1] = Q[n];

	for (i = n-2; i >= 0; i-- )
		X[i] = Q[i+1] + P[i+1]*X[i+1];
}

void iteration(double A[MAX_SIZE][MAX_SIZE],
							 double B[MAX_SIZE],
						 	 double P[MAX_SIZE],
						 	 double Q[MAX_SIZE],
						 	 double X[MAX_SIZE],
						 	 int n)
{
	forward_iteration(A, B, P, Q, n);
	reverse_iteration(P, Q, X, n);
}


int main()
{
	double A[MAX_SIZE][MAX_SIZE], L[MAX_SIZE][MAX_SIZE],
         U[MAX_SIZE][MAX_SIZE], R[MAX_SIZE][MAX_SIZE],
         B[MAX_SIZE], X[MAX_SIZE], P[MAX_SIZE],
				 Q[MAX_SIZE], eps = 0.01;

 	ifstream fin("input.txt");
	ofstream fout("output.txt");

	fout.precision(5);
	cout.precision(5);

	int n = input_conjugated_matrix(A, B, fin);

	iteration(A, B, P, Q, X, n);

	print_matrix_to_file(A, fout, n, n, "Input");
	print_vector_to_file(P, fout, n+1, "vector P");
	print_vector_to_file(Q, fout, n+1, "vector Q");
	print_vector_to_file(X, fout, n, "vector X");

	fin.close();
	fout.close();
	return 1;
}
